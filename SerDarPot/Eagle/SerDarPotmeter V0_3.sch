<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.025" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="danny_lbr">
<packages>
<package name="3382G">
<smd name="22" x="0" y="6.2" dx="1.8" dy="2" layer="1"/>
<smd name="21" x="0" y="-7.7" dx="1.8" dy="2" layer="1"/>
<smd name="1" x="-2.5" y="-7.7" dx="1.8" dy="2" layer="1"/>
<smd name="3" x="2.5" y="-7.7" dx="1.8" dy="2" layer="1"/>
<wire x1="-5.5" y1="-6.5" x2="5.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-3.2" y1="5.54" x2="-1.1" y2="5.54" width="0.127" layer="21"/>
<wire x1="1.1" y1="5.54" x2="3.2" y2="5.54" width="0.127" layer="21"/>
<wire x1="-5.5" y1="1.96" x2="-5.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="1.96" x2="5.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="1.96" x2="-3.2" y2="5.54" width="0.127" layer="21"/>
<wire x1="5.5" y1="1.96" x2="3.2" y2="5.54" width="0.127" layer="21"/>
<hole x="-4.25" y="-5.49" drill="1.2"/>
<hole x="4.25" y="-5.49" drill="1.2"/>
<text x="6.35" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="5.8"/>
</package>
<package name="WR_WTB_3_1_25">
<smd name="GND2" x="3.8" y="0" dx="2.1" dy="3" layer="1"/>
<smd name="GND1" x="-3.8" y="0" dx="2.1" dy="3" layer="1"/>
<smd name="3" x="-1.25" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<smd name="2" x="0" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<smd name="1" x="1.25" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<wire x1="-4.3" y1="2.8" x2="-4.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="4.3" y1="2.8" x2="4.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.4" x2="2.5" y2="-1.4" width="0.127" layer="21"/>
<circle x="3.6" y="2.2" radius="0.3" width="0.127" layer="21"/>
<wire x1="-4.3" y1="2.8" x2="-1.9" y2="2.8" width="0.127" layer="21"/>
<wire x1="1.9" y1="2.8" x2="4.3" y2="2.8" width="0.127" layer="21"/>
<text x="5.08" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="5.08" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WR_WTB_4_1_12">
<smd name="GND2" x="4.425" y="0" dx="2.1" dy="3" layer="1"/>
<smd name="GND1" x="-4.425" y="0" dx="2.1" dy="3" layer="1"/>
<smd name="4" x="-1.875" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<smd name="3" x="-0.625" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<smd name="1" x="1.875" y="2.9" dx="0.81" dy="1.6" layer="1"/>
<wire x1="-4.925" y1="2.8" x2="-4.925" y2="1.8" width="0.127" layer="21"/>
<wire x1="4.925" y1="2.8" x2="4.925" y2="1.8" width="0.127" layer="21"/>
<wire x1="-3.125" y1="-1.4" x2="3.125" y2="-1.4" width="0.127" layer="21"/>
<circle x="4.225" y="2.2" radius="0.3" width="0.127" layer="21"/>
<wire x1="-4.925" y1="2.8" x2="-2.525" y2="2.8" width="0.127" layer="21"/>
<wire x1="2.525" y1="2.8" x2="4.925" y2="2.8" width="0.127" layer="21"/>
<text x="5.705" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="5.705" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<smd name="2" x="0.625" y="2.9" dx="0.81" dy="1.6" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="POTM">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="2.667" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.778" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="2.54"/>
<vertex x="2.667" y="3.81"/>
<vertex x="1.778" y="2.921"/>
</polygon>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="-0.508" y="0.762" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0.254" y="-2.413" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="3382" prefix="POT">
<gates>
<gate name="G$1" symbol="POTM" x="0" y="0"/>
</gates>
<devices>
<device name="G" package="3382G">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="21"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WR_WTB_3_1_25" prefix="CON">
<gates>
<gate name="-1" symbol="M" x="-5.08" y="20.32"/>
<gate name="-2" symbol="M" x="-5.08" y="17.78"/>
<gate name="-3" symbol="M" x="-5.08" y="15.24"/>
<gate name="-GND1" symbol="M" x="-5.08" y="12.7"/>
<gate name="-GND2" symbol="M" x="-5.08" y="10.16"/>
</gates>
<devices>
<device name="HORZ" package="WR_WTB_3_1_25">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-GND1" pin="S" pad="GND1"/>
<connect gate="-GND2" pin="S" pad="GND2"/>
</connects>
<technologies>
<technology name="">
<attribute name="OC_FARNELL" value="1841416" constant="no"/>
<attribute name="OC_SUPP" value="653103124022" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WR_WTB_4_1_25" prefix="CON">
<gates>
<gate name="-1" symbol="M" x="-5.08" y="20.32"/>
<gate name="-2" symbol="M" x="-5.08" y="15.24"/>
<gate name="-3" symbol="M" x="-5.08" y="10.16"/>
<gate name="-4" symbol="M" x="-5.08" y="5.08"/>
<gate name="-GND1" symbol="M" x="-5.08" y="0"/>
<gate name="-GND2" symbol="M" x="-5.08" y="-5.08"/>
</gates>
<devices>
<device name="HORZ" package="WR_WTB_4_1_12">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-GND1" pin="S" pad="GND1"/>
<connect gate="-GND2" pin="S" pad="GND2"/>
</connects>
<technologies>
<technology name="">
<attribute name="OC_FARNELL" value="1841418" constant="no"/>
<attribute name="OC_SUPP" value="653104124022" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="POT1" library="danny_lbr" deviceset="3382" device="G"/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="CON1" library="danny_lbr" deviceset="WR_WTB_3_1_25" device="HORZ">
<attribute name="OC_FARNELL" value="1841416"/>
</part>
<part name="CON2" library="danny_lbr" deviceset="WR_WTB_4_1_25" device="HORZ">
<attribute name="OC_FARNELL" value="1841418"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="POT1" gate="G$1" x="100.33" y="99.06" smashed="yes" rot="MR270">
<attribute name="NAME" x="102.87" y="102.87" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="105.41" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="P+1" gate="1" x="119.38" y="118.11" smashed="yes">
<attribute name="VALUE" x="116.84" y="118.11" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="100.33" y="107.95" smashed="yes">
<attribute name="VALUE" x="97.79" y="107.95" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="100.33" y="90.17"/>
<instance part="GND2" gate="1" x="119.38" y="76.2"/>
<instance part="CON1" gate="-1" x="127" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="93.218" size="1.778" layer="95"/>
</instance>
<instance part="CON1" gate="-2" x="127" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="105.918" size="1.778" layer="95"/>
</instance>
<instance part="CON1" gate="-3" x="127" y="113.03" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="112.268" size="1.778" layer="95"/>
</instance>
<instance part="CON1" gate="-GND1" x="127" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="88.138" size="1.778" layer="95"/>
</instance>
<instance part="CON1" gate="-GND2" x="127" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="83.058" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-1" x="127" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="98.298" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-2" x="127" y="110.49" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="109.728" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-3" x="127" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="103.378" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-4" x="127" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="90.678" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-GND1" x="127" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="85.598" size="1.778" layer="95"/>
</instance>
<instance part="CON2" gate="-GND2" x="127" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="127.508" y="80.518" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="119.38" y1="88.9" x2="119.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="119.38" y1="86.36" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="119.38" y1="83.82" x2="119.38" y2="81.28" width="0.1524" layer="91"/>
<wire x1="119.38" y1="81.28" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="81.28" x2="119.38" y2="81.28" width="0.1524" layer="91"/>
<wire x1="121.92" y1="83.82" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="121.92" y1="86.36" x2="119.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="121.92" y1="88.9" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CON1" gate="-GND1" pin="S"/>
<pinref part="CON1" gate="-GND2" pin="S"/>
<pinref part="CON2" gate="-GND1" pin="S"/>
<pinref part="CON2" gate="-GND2" pin="S"/>
<wire x1="119.38" y1="88.9" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CON2" gate="-4" pin="S"/>
<wire x1="119.38" y1="91.44" x2="119.38" y2="93.98" width="0.1524" layer="91"/>
<wire x1="121.92" y1="91.44" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CON1" gate="-1" pin="S"/>
<wire x1="121.92" y1="93.98" x2="119.38" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="100.33" y1="93.98" x2="100.33" y2="92.71" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="119.38" y1="110.49" x2="119.38" y2="113.03" width="0.1524" layer="91"/>
<wire x1="119.38" y1="113.03" x2="119.38" y2="115.57" width="0.1524" layer="91"/>
<pinref part="CON1" gate="-3" pin="S"/>
<wire x1="121.92" y1="113.03" x2="119.38" y2="113.03" width="0.1524" layer="91"/>
<pinref part="CON2" gate="-2" pin="S"/>
<wire x1="121.92" y1="110.49" x2="119.38" y2="110.49" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="POT1" gate="G$1" pin="1"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="100.33" y1="104.14" x2="100.33" y2="105.41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="121.92" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<wire x1="119.38" y1="106.68" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="CON1" gate="-2" pin="S"/>
<pinref part="CON2" gate="-3" pin="S"/>
<wire x1="121.92" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="POT1" gate="G$1" pin="3"/>
<pinref part="CON2" gate="-1" pin="S"/>
<wire x1="105.41" y1="99.06" x2="121.92" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
