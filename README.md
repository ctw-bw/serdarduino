# SerDarDuino

This is hardware developed in the [EU Script project](http://rehabilitationrobotics.net/cms3/) to control a prototype of a hand orthosis.

It consists of a modified Arduino Due design, with added powerswitch (to switch on supply to servo and motor), electrical isolation and lots of connectors. Later in the project a motor driver extention was developed, this is also in this repository.


For more information contact v.i.sluiter@utwente.nl