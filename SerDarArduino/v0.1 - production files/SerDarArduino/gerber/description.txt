*.DIM - Dimension layer
*.GTL - Top layer copper
*.GBL - Bottom layer copper
*.GTO - Top Silkscreen
*.GBO - Bottom silkscreen
*.GTS - Top soldermask
*.GBS - Bottom soldermask
*.TXT - Drill file excellon
 
PCB Name: SerDarArduino V0.1
PCB Dimensions: 100mm x 54.5mm
PCB Thickness: 1.55mm
PCB Minimal track width: 0.15mm
PCB Minimal drill diameter: 0.25mm
PCB Copper thickness: 35um
PCB Surface finish: ENIG Gold
PCB Solder Resist: Top+Bottom
PCB Silk Screen: Top+Bottom
PCB E-Test: No
PCB Quantity: 12 ?
PCB Overproduction: Yes please
PCB Production Time: 8 working days
PCB Shipping: DHL